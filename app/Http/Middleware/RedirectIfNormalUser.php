<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class RedirectIfNormalUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check()){
			if(Auth::user()->user_group == '1'){
				return redirect('/');
			}
			else{
				return $next($request);
			}
		}
		else{
			return redirect()->action('Backend\LoginController@index');
		}
    }
}
