<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Backend\State;

class StateController extends Controller
{
    public function __construct()
    {
		$this->state = new State();
    }
    public function index()
    {
		$states = $this->state->listState();
		return view('backend/states/list',['states'=>$states]);
    }
    public function add()
    {
		return view('backend/states/add');
    }
    public function save(Request $request)
    {
		$data['user_id'] = Auth::id();
		$data['name'] = $request->input('name');
		$data['short_name'] = $request->input('short_name');
		$data['sort_order'] = $request->input('sort_order');
		
		$this->validate($request,[
			'name'=>'required|max:255',
			'short_name'=>'nullable|max:255',
			'sort_order'=>'nullable|integer',
		]);
		
		$result = $this->state->addState($data);
		
		if($result){
			$request->session()->flash('success', 'Record added successfully!');
		}
		else{
			$request->session()->flash('error', 'Something went wrong!');
		}
		return redirect()->back();
    }
    public function view($id)
    {
		$state = $this->state->viewState($id);
		return view('backend/states/view',['state'=>$state]);
    }
    public function edit($id)
    {
		$state = $this->state->editState($id);
		return view('backend/states/edit',['state'=>$state]);
    }
    public function update(Request $request,$id)
    {
		$data['user_id'] = Auth::id();
		$data['name'] = $request->input('name');
		$data['short_name'] = $request->input('short_name');
		$data['sort_order'] = $request->input('sort_order');
		
		$this->validate($request,[
			'name'=>'required|max:255',
			'short_name'=>'nullable|max:255',
			'sort_order'=>'nullable|integer',
		]);
		
		$result = $this->state->updateState($id,$data);
		
		if($result){
			$request->session()->flash('success', 'Record updated successfully!');
		}
		else{
			$request->session()->flash('error', 'Something went wrong!');
		}
		return redirect()->back();
    }
    public function delete(Request $request,$id)
    {
		$result = $this->state->deleteState($id);
		if($result){
			$request->session()->flash('success', 'Record deleted successfully!');
		}
		else{
			$request->session()->flash('error', 'Something went wrong!');
		}
		return redirect()->back();
    }
}
