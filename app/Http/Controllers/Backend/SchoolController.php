<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Backend\School;
use App\Backend\State;

class SchoolController extends Controller
{
    public function __construct()
    {
		$this->school = new School();
		$this->state = new State();
    }
    public function index()
    {
		$schools = $this->school->listSchool();
		return view('backend/schools/list',['schools'=>$schools]);
    }
    public function add()
    {
		$states = $this->state->listState();
		return view('backend/schools/add',['states'=>$states]);
    }
    public function save(Request $request)
    {
		$data['user_id'] = Auth::id();
		$data['name'] = $request->input('name');
		$data['email'] = $request->input('email');
		$data['password'] = $request->input('password');
		$data['phone'] = $request->input('phone');
		$data['mobile'] = $request->input('mobile');
		$data['website'] = $request->input('website');
		$data['state'] = $request->input('state');
		$data['city'] = $request->input('city');
		$data['address'] = $request->input('address');
		
		$this->validate($request,[
			'name'=>'required|max:255',
			'email'=>'required|email|max:255',
			'password'=>'required|min:6',
			'phone'=>'required|max:255',
			'website'=>'required|max:255',
			'state'=>'required',
			'city'=>'required|max:255',
			'address'=>'required',
		]);
		
		$result = $this->school->addSchool($data);
		
		if($result){
			$request->session()->flash('success', 'Record added successfully!');
		}
		else{
			$request->session()->flash('error', 'Something went wrong!');
		}
		return redirect()->back();
    }
    public function view($id)
    {
		$school = $this->school->viewSchool($id);
		return view('backend/schools/view',['school'=>$school]);
    }
    public function edit($id)
    {
		$school = $this->school->editSchool($id);
		$states = $this->state->listState();
		return view('backend/schools/edit',['school'=>$school,'states'=>$states]);
    }
    public function update(Request $request,$id)
    {
		$data['user_id'] = Auth::id();
		$data['name'] = $request->input('name');
		$data['email'] = $request->input('email');
		$data['phone'] = $request->input('phone');
		$data['mobile'] = $request->input('mobile');
		$data['website'] = $request->input('website');
		$data['state'] = $request->input('state');
		$data['city'] = $request->input('city');
		$data['address'] = $request->input('address');
		
		$this->validate($request,[
			'name'=>'required|max:255',
			'email'=>'required|email|max:255',
			'phone'=>'required|max:255',
			'website'=>'required|max:255',
			'state'=>'required',
			'city'=>'required|max:255',
			'address'=>'required',
		]);
		
		$result = $this->school->updateSchool($id,$data);
		
		if($result){
			$request->session()->flash('success', 'Record updated successfully!');
		}
		else{
			$request->session()->flash('error', 'Something went wrong!');
		}
		return redirect()->back();
    }
    public function delete(Request $request,$id)
    {
		$result = $this->school->deleteSchool($id);
		if($result){
			$request->session()->flash('success', 'Record deleted successfully!');
		}
		else{
			$request->session()->flash('error', 'Something went wrong!');
		}
		return redirect()->back();
    }
    public function block(Request $request,$id)
    {
		$result = $this->school->blockSchool($id);
		if($result){
			$request->session()->flash('success', 'Record updated successfully!');
		}
		else{
			$request->session()->flash('error', 'Something went wrong!');
		}
		return redirect()->back();
    }
    public function unblock(Request $request,$id)
    {
		$result = $this->school->unblockSchool($id);
		if($result){
			$request->session()->flash('success', 'Record updated successfully!');
		}
		else{
			$request->session()->flash('error', 'Something went wrong!');
		}
		return redirect()->back();
    }
}
