<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use DB;
use Auth;
use File;
use Carbon\Carbon;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class EditprofileController extends Controller
{
    public function index()
    {
		$user_id = Auth::id();
		$profile = DB::select('select * from users where id = ?',[$user_id]);
		return view('backend/editprofile',['profile'=>$profile]);
    }
    public function save(Request $request)
    {
		DB::transaction(function () use($request) {
		    $name = $request->input('name');
			$email = $request->input('email');
			$mobile = $request->input('mobile');
			$image = $request->file('image');
			$user_id = Auth::id();
			$updated_at = Carbon::now('Asia/Kolkata');

			$this->validate($request,[
				'name'=>'required',
				'email'=>'required|email',
				'image' => 'image|max:2048'
			]);
			
			if($image){
			    $image_name = 'profile.png';
			    $image->move(public_path('backend/uploads/'.$user_id.md5(Auth::user()->name)), $image_name);
			    $res = DB::update('update users set name = ?,email = ?,mobile = ?,thumb = ?,avatar = ?,updated_at = ? where id = ?',[$name,$email,$mobile,$image_name,$image_name,$updated_at,$user_id]);
			}
			else{
				$res = DB::update('update users set name = ?,email = ?,mobile = ?,updated_at = ? where id = ?',[$name,$email,$mobile,$updated_at,$user_id]);
			}
			
			if($res){
				$request->session()->flash('success', 'Your Profile has been successfully updated.');
			}
			else{
				$request->session()->flash('error', 'Something went wrong.');
			}

		});
		
    	return redirect()->back();
    }
}
