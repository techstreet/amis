<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Backend\Classs;
use App\Backend\Section;

class ClasssController extends Controller
{
    public function __construct()
    {
		$this->classs = new Classs();
		$this->section = new Section();
    }
    public function index()
    {
		$classes = $this->classs->listClasss();
		return view('backend/classes/list',['classes'=>$classes]);
    }
    public function add()
    {
		$sections = $this->section->listSection();
		return view('backend/classes/add',['sections'=>$sections]);
    }
    public function save(Request $request)
    {
		$data['user_id'] = Auth::id();
		$data['school_id'] = Auth::user()->school_id;
		$data['name'] = $request->input('name');
		$data['description'] = $request->input('description');
		$data['sections'] = $request->input('sections');
		
		if(!empty($data['sections'])){
			$data['sections'] = implode(",",$data['sections']);
		}
		
		$this->validate($request,[
			'name'=>'required|max:255',
			'description'=>'nullable|max:255',
			'sections'=>'nullable',
		]);
		
		$result = $this->classs->addClasss($data);
		
		if($result){
			$request->session()->flash('success', 'Record added successfully!');
		}
		else{
			$request->session()->flash('error', 'Something went wrong!');
		}
		return redirect()->back();
    }
    public function view($id)
    {
		$classs = $this->classs->viewClasss($id);
		return view('backend/classes/view',['classs'=>$classs]);
    }
    public function edit($id)
    {
		$classs = $this->classs->editClasss($id);
		$sections = $this->section->listSection();
		return view('backend/classes/edit',['classs'=>$classs,'sections'=>$sections]);
    }
    public function update(Request $request,$id)
    {
		$data['user_id'] = Auth::id();
		$data['school_id'] = Auth::user()->school_id;
		$data['name'] = $request->input('name');
		$data['description'] = $request->input('description');
		$data['sections'] = $request->input('sections');
		
		if(!empty($data['sections'])){
			$data['sections'] = implode(",",$data['sections']);
		}
		
		$this->validate($request,[
			'name'=>'required|max:255',
			'description'=>'nullable|max:255',
			'sections'=>'nullable',
		]);
		
		$result = $this->classs->updateClasss($id,$data);
		
		if($result){
			$request->session()->flash('success', 'Record updated successfully!');
		}
		else{
			$request->session()->flash('error', 'Something went wrong!');
		}
		return redirect()->back();
    }
    public function delete(Request $request,$id)
    {
		$result = $this->classs->deleteClasss($id);
		if($result){
			$request->session()->flash('success', 'Record deleted successfully!');
		}
		else{
			$request->session()->flash('error', 'Something went wrong!');
		}
		return redirect()->back();
    }
}
