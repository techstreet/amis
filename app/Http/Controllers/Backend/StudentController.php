<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Backend\Student;
use App\Backend\Session;
use App\Backend\Classs;
use App\Backend\Section;
use App\Backend\State;

class StudentController extends Controller
{
    public function __construct()
    {
		$this->student = new Student();
		$this->session = new Session();
		$this->classs = new Classs();
		$this->section = new Section();
		$this->state = new State();
    }
    public function index()
    {
		$students = $this->student->listStudent();
		return view('backend/students/list',['students'=>$students]);
    }
    public function add()
    {
		$sessions = $this->session->listSession();
		$classes = $this->classs->listClasss();
		$sections = $this->section->listSection();
		$states = $this->state->listState();
		return view('backend/students/add',['sessions'=>$sessions,'classes'=>$classes,'sections'=>$sections,'states'=>$states]);
    }
    public function save(Request $request)
    {
		$data['user_id'] = Auth::id();
		$data['school_id'] = Auth::user()->school_id;
		$data['name'] = $request->input('name');
		$data['roll_no'] = $request->input('roll_no');
		$data['registration_no'] = $request->input('registration_no');
		$data['registration_date'] = $request->input('registration_date');
		$data['session'] = $request->input('session');
		$data['classs'] = $request->input('classs');
		$data['section'] = $request->input('section');
		$data['dob'] = $request->input('dob');
		$data['blood_group'] = $request->input('blood_group');
		$data['state'] = $request->input('state');
		$data['city'] = $request->input('city');
		$data['address'] = $request->input('address');
		$data['pincode'] = $request->input('pincode');
		$data['father_name'] = $request->input('father_name');
		$data['mother_name'] = $request->input('mother_name');
		$data['parent_email'] = $request->input('parent_email');
		$data['parent_mobile'] = $request->input('parent_mobile');
		
		$this->validate($request,[
			'name'=>'nullable|required',
			'roll_no'=>'nullable|required',
			'registration_no'=>'nullable|required',
			'registration_date'=>'nullable|required|date',
			'session'=>'nullable|required',
			'classs'=>'nullable|required',
			'section'=>'nullable|required',
			'dob'=>'nullable|required|date',
			'blood_group'=>'nullable|required',
			'state'=>'nullable|required',
			'city'=>'nullable|required',
			'address'=>'nullable|required',
			'pincode'=>'nullable|required',
			'father_name'=>'nullable|required',
			'mother_name'=>'nullable|required',
			'parent_email'=>'nullable|required',
			'parent_mobile'=>'nullable|required',
		]);
		
		$result = $this->student->addStudent($data);
		
		if($result){
			$request->session()->flash('success', 'Record added successfully!');
		}
		else{
			$request->session()->flash('error', 'Something went wrong!');
		}
		return redirect()->back();
    }
    public function view($id)
    {
		$student = $this->student->viewStudent($id);
		return view('backend/students/view',['student'=>$student]);
    }
    public function edit($id)
    {
		$student = $this->student->editStudent($id);
		$sessions = $this->session->listSession();
		$classes = $this->classs->listClasss();
		$sections = $this->section->listSection();
		$states = $this->state->listState();
		return view('backend/students/edit',['student'=>$student,'sessions'=>$sessions,'classes'=>$classes,'sections'=>$sections,'states'=>$states]);
    }
    public function update(Request $request,$id)
    {
		$data['user_id'] = Auth::id();
		$data['school_id'] = Auth::user()->school_id;
		$data['name'] = $request->input('name');
		$data['roll_no'] = $request->input('roll_no');
		$data['registration_no'] = $request->input('registration_no');
		$data['registration_date'] = $request->input('registration_date');
		$data['session'] = $request->input('session');
		$data['classs'] = $request->input('classs');
		$data['section'] = $request->input('section');
		$data['dob'] = $request->input('dob');
		$data['blood_group'] = $request->input('blood_group');
		$data['state'] = $request->input('state');
		$data['city'] = $request->input('city');
		$data['address'] = $request->input('address');
		$data['pincode'] = $request->input('pincode');
		$data['father_name'] = $request->input('father_name');
		$data['mother_name'] = $request->input('mother_name');
		$data['parent_email'] = $request->input('parent_email');
		$data['parent_mobile'] = $request->input('parent_mobile');
		
		$this->validate($request,[
			'name'=>'nullable|required',
			'roll_no'=>'nullable|required',
			'registration_no'=>'nullable|required',
			'registration_date'=>'nullable|required|date',
			'session'=>'nullable|required',
			'classs'=>'nullable|required',
			'section'=>'nullable|required',
			'dob'=>'nullable|required|date',
			'blood_group'=>'nullable|required',
			'state'=>'nullable|required',
			'city'=>'nullable|required',
			'address'=>'nullable|required',
			'pincode'=>'nullable|required',
			'father_name'=>'nullable|required',
			'mother_name'=>'nullable|required',
			'parent_email'=>'nullable|required',
			'parent_mobile'=>'nullable|required',
		]);
		
		$result = $this->student->updateStudent($id,$data);
		
		if($result){
			$request->session()->flash('success', 'Record updated successfully!');
		}
		else{
			$request->session()->flash('error', 'Something went wrong!');
		}
		return redirect()->back();
    }
    public function delete(Request $request,$id)
    {
		$result = $this->student->deleteStudent($id);
		if($result){
			$request->session()->flash('success', 'Record deleted successfully!');
		}
		else{
			$request->session()->flash('error', 'Something went wrong!');
		}
		return redirect()->back();
    }
}
