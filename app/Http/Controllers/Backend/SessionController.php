<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Backend\Session;

class SessionController extends Controller
{
    public function __construct()
    {
		$this->session = new Session();
    }
    public function index()
    {
		$sessions = $this->session->listSession();
		return view('backend/sessions/list',['sessions'=>$sessions]);
    }
    public function add()
    {
		return view('backend/sessions/add');
    }
    public function save(Request $request)
    {
		$data['user_id'] = Auth::id();
		$data['school_id'] = Auth::user()->school_id;
		$data['name'] = $request->input('name');
		$data['description'] = $request->input('description');
		
		$this->validate($request,[
			'name'=>'required|max:255',
			'description'=>'nullable|max:255',
		]);
		
		$result = $this->session->addSession($data);
		
		if($result){
			$request->session()->flash('success', 'Record added successfully!');
		}
		else{
			$request->session()->flash('error', 'Something went wrong!');
		}
		return redirect()->back();
    }
    public function view($id)
    {
		$session = $this->session->viewSession($id);
		return view('backend/sessions/view',['session'=>$session]);
    }
    public function edit($id)
    {
		$session = $this->session->editSession($id);
		return view('backend/sessions/edit',['session'=>$session]);
    }
    public function update(Request $request,$id)
    {
		$data['user_id'] = Auth::id();
		$data['school_id'] = Auth::user()->school_id;
		$data['name'] = $request->input('name');
		$data['description'] = $request->input('description');
		
		$this->validate($request,[
			'name'=>'required|max:255',
			'description'=>'nullable|max:255',
		]);
		
		$result = $this->session->updateSession($id,$data);
		
		if($result){
			$request->session()->flash('success', 'Record updated successfully!');
		}
		else{
			$request->session()->flash('error', 'Something went wrong!');
		}
		return redirect()->back();
    }
    public function delete(Request $request,$id)
    {
		$result = $this->session->deleteSession($id);
		if($result){
			$request->session()->flash('success', 'Record deleted successfully!');
		}
		else{
			$request->session()->flash('error', 'Something went wrong!');
		}
		return redirect()->back();
    }
}
