<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Backend\Device;

class DeviceController extends Controller
{
    public function __construct()
    {
		$this->device = new Device();
    }
    public function index()
    {
		$devices = $this->device->listDevice();
		return view('backend/devices/list',['devices'=>$devices]);
    }
    public function add()
    {
		return view('backend/devices/add');
    }
    public function save(Request $request)
    {
		$data['user_id'] = Auth::id();
		$data['school_id'] = Auth::user()->school_id;
		$data['name'] = $request->input('name');
		$data['description'] = $request->input('description');
		
		$this->validate($request,[
			'name'=>'required|max:255',
			'description'=>'nullable|max:255',
		]);
		
		$result = $this->device->addDevice($data);
		
		if($result){
			$request->session()->flash('success', 'Record added successfully!');
		}
		else{
			$request->session()->flash('error', 'Something went wrong!');
		}
		return redirect()->back();
    }
    public function view($id)
    {
		$device = $this->device->viewDevice($id);
		return view('backend/devices/view',['device'=>$device]);
    }
    public function edit($id)
    {
		$device = $this->device->editDevice($id);
		return view('backend/devices/edit',['device'=>$device]);
    }
    public function update(Request $request,$id)
    {
		$data['user_id'] = Auth::id();
		$data['school_id'] = Auth::user()->school_id;
		$data['name'] = $request->input('name');
		$data['description'] = $request->input('description');
		
		$this->validate($request,[
			'name'=>'required|max:255',
			'description'=>'nullable|max:255',
		]);
		
		$result = $this->device->updateDevice($id,$data);
		
		if($result){
			$request->session()->flash('success', 'Record updated successfully!');
		}
		else{
			$request->session()->flash('error', 'Something went wrong!');
		}
		return redirect()->back();
    }
    public function delete(Request $request,$id)
    {
		$result = $this->device->deleteDevice($id);
		if($result){
			$request->session()->flash('success', 'Record deleted successfully!');
		}
		else{
			$request->session()->flash('error', 'Something went wrong!');
		}
		return redirect()->back();
    }
}
