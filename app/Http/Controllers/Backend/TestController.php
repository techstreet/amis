<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use DB;
use Artisan;
use App;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class TestController extends Controller
{
    public function __construct()
    {
		$this->date = Carbon::now(config('app.timezone'));
    }
    public function index()
    {
		DB::statement("CREATE DATABASE IF NOT EXISTS `brandstu_test`");
		DB::statement("CREATE USER IF NOT EXISTS 'brandstu_test'@'localhost' IDENTIFIED BY 'Test@123'");
		DB::statement("GRANT ALL PRIVILEGES ON `brandstu_test`.* TO 'brandstu_test'@'localhost'");

		$new_connection = 'new';

		config(["database.connections.$new_connection" => [
		        "driver" => "mysql",
		        "host" => "",
		        "port" => "",
		        "database" => "brandstu_test",
		        "username" => "brandstu_test",
		        "password" => "Test@123",
		        "charset" => "utf8",
		        "collation" => "utf8_unicode_ci",
		        "prefix" => "",
		        "strict" => false,
		        "engine" => 'InnoDB'
		]]);

		Artisan::call('migrate', ['--database' => $new_connection ]);
		
    }
}
