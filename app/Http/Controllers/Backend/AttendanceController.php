<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Backend\Attendance;

class AttendanceController extends Controller
{
    public function __construct()
    {
		$this->attendance = new Attendance();
		$this->date = Carbon::now(config('app.timezone'));
    }
    public function index()
    {
		$attendances = $this->attendance->listAttendance();
		return view('backend/attendances/list',['attendances'=>$attendances,'attendance_date'=>$this->date]);
    }
    public function view($id)
    {
		$attendance = $this->attendance->viewAttendance($id);
		return view('backend/attendances/view',['attendance'=>$attendance]);
    }
    public function cancel(Request $request,$id)
    {
		$result = $this->attendance->cancelAttendance($id);
		if($result){
			$request->session()->flash('success', 'Record cancelled successfully!');
		}
		else{
			$request->session()->flash('error', 'Something went wrong!');
		}
		return redirect()->back();
    }
    public function active(Request $request,$id)
    {
		$result = $this->attendance->activeAttendance($id);
		if($result){
			$request->session()->flash('success', 'Record activated successfully!');
		}
		else{
			$request->session()->flash('error', 'Something went wrong!');
		}
		return redirect()->back();
    }
}
