<?php

namespace App\Backend;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Http\Requests;
use Carbon\Carbon;

class Section extends Model
{
	public function __construct()
    {
		$this->date = Carbon::now('Asia/Kolkata');
    }
    public function listSection()
	{
		$school_id = Auth::user()->school_id;
		return DB::table('sections')
			->where([
			['school_id', $school_id],
			['status', '1'],
			])
			->orderBy('name')
            ->get();
	}
	public function addSection($data)
	{
		$user_id = $data['user_id'];
		$school_id = $data['school_id'];
		$name = $data['name'];
		$description = $data['description'];
			
		return DB::table('sections')->insert(
		    ['school_id' => $school_id,'name' => $name,'description' => $description,'created_at' => $this->date,'created_by' => $user_id]
		);
	}
	public function viewSection($id)
	{
		$school_id = Auth::user()->school_id;
		$sections = DB::table('sections')
			->where([
			['id', $id],
			['school_id', $school_id],
			['status', '1'],
			])
            ->first();
    	if(count($sections)>0){
			return $sections;
		}
		else{
			abort(404);
		}
	}
	public function editSection($id)
	{
		$school_id = Auth::user()->school_id;
		$sections = DB::table('sections')
			->where([
			['id', $id],
			['school_id', $school_id],
			['status', '1'],
			])
            ->first();
        if(count($sections)>0){
			return $sections;
		}
		else{
			abort(404);
		}
	}
	public function updateSection($id,$data)
	{
		$user_id = $data['user_id'];
		$school_id = $data['school_id'];
		$name = $data['name'];
		$description = $data['description'];
		
		return DB::table('sections')
            ->where('id', $id)
            ->update(['school_id' => $school_id,'name' => $name,'description' => $description,'updated_at' => $this->date,'updated_by' => $user_id]);
            
	}
	public function deleteSection($id)
	{
		$user_id = Auth::id();
		$school_id = Auth::user()->school_id;
		$sections = DB::table('sections')
			->where([
			['id', $id],
			['school_id', $school_id],
			['status', '1'],
			])
            ->first();
    	if(count($sections)>0){
			return DB::table('sections')
	            ->where('id', $id)
	            ->update(['status' => '0','updated_at' => $this->date,'updated_by' => $user_id]);
		}
		else{
			abort(404);
		}
	}
}
