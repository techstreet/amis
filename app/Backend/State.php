<?php

namespace App\Backend;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Http\Requests;
use Carbon\Carbon;

class State extends Model
{
	public function __construct()
    {
		$this->date = Carbon::now('Asia/Kolkata');
    }
    public function listState()
	{
		return DB::table('states')
			->where([
			['status', '1'],
			])
			->orderBy('sort_order')
            ->get();
	}
	public function addState($data)
	{
		$user_id = $data['user_id'];
		$name = $data['name'];
		$short_name = $data['short_name'];
		$sort_order = $data['sort_order'];
			
		return DB::table('states')->insert(
		    ['name' => $name,'short_name' => $short_name,'sort_order' => $sort_order,'created_at' => $this->date,'created_by' => $user_id]
		);
	}
	public function viewState($id)
	{
		$states = DB::table('states')
			->where([
			['id', $id],
			['status', '1'],
			])
            ->first();
        if(count($states)>0){
			return $states;
		}
		else{
			abort(404);
		}
	}
	public function editState($id)
	{
		$states = DB::table('states')
			->where([
			['id', $id],
			['status', '1'],
			])
            ->first();
        if(count($states)>0){
			return $states;
		}
		else{
			abort(404);
		}
	}
	public function updateState($id,$data)
	{
		$user_id = $data['user_id'];
		$name = $data['name'];
		$short_name = $data['short_name'];
		$sort_order = $data['sort_order'];
		
		return DB::table('states')
            ->where('id', $id)
            ->update(['name' => $name,'short_name' => $short_name,'sort_order' => $sort_order,'updated_at' => $this->date,'updated_by' => $user_id]);
            
	}
	public function deleteState($id)
	{
		$user_id = Auth::id();
		$states = DB::table('states')
			->where([
			['id', $id],
			['status', '1'],
			])
            ->first();
    	if(count($states)>0){
			return DB::table('states')
	            ->where('id', $id)
	            ->update(['status' => '0','updated_at' => $this->date,'updated_by' => $user_id]);
		}
		else{
			abort(404);
		}
	}
}
