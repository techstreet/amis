<?php

namespace App\Backend;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Http\Requests;
use Carbon\Carbon;

class Student extends Model
{
	public function __construct()
    {
		$this->date = Carbon::now('Asia/Kolkata');
    }
    public function listStudent()
	{
		$school_id = Auth::user()->school_id;
		return DB::table('students')
			->select('students.*','classes.name as class_name','sections.name as section_name','sessions.name as session_name')
			->leftJoin('classes','classes.id','=','students.class_id')
			->leftJoin('sections','sections.id','=','students.section_id')
			->leftJoin('sessions','sessions.id','=','students.session_id')
			->where([
			['students.school_id', $school_id],
			['students.status', '1'],
			])
			->orderBy('students.name')
            ->get();
	}
	public function addStudent($data)
	{
		$user_id = $data['user_id'];
		$school_id = $data['school_id'];
		$name = $data['name'];
		$roll_no = $data['roll_no'];
		$registration_no = $data['registration_no'];
		$registration_date = date_format(date_create($data['registration_date']),"Y-m-d");
		$session = $data['session'];
		$classs = $data['classs'];
		$section = $data['section'];
		$dob = date_format(date_create($data['dob']),"Y-m-d");
		$blood_group = $data['blood_group'];
		$state = $data['state'];
		$city = $data['city'];
		$address = $data['address'];
		$pincode = $data['pincode'];
		$father_name = $data['father_name'];
		$mother_name = $data['mother_name'];
		$parent_email = $data['parent_email'];
		$parent_mobile = $data['parent_mobile'];
			
		return DB::table('students')->insert(
		    ['school_id' => $school_id,'name' => $name,'roll_no' => $roll_no,'registration_no' => $registration_no,'registration_date' => $registration_date,'session_id' => $session,'class_id' => $classs,'section_id' => $section,'dob' => $dob,'blood_group' => $blood_group,'state_id' => $state,'city' => $city,'address' => $address,'pincode' => $pincode,'father_name' => $father_name,'mother_name' => $mother_name,'parent_email' => $parent_email,'parent_mobile' => $parent_mobile,'created_at' => $this->date,'created_by' => $user_id]
		);
	}
	public function viewStudent($id)
	{
		$school_id = Auth::user()->school_id;
		$students = DB::table('students')
			->select('students.*','classes.name as class_name','sections.name as section_name','sessions.name as session_name','states.name as state_name')
			->leftJoin('classes','classes.id','=','students.class_id')
			->leftJoin('sections','sections.id','=','students.section_id')
			->leftJoin('sessions','sessions.id','=','students.session_id')
			->leftJoin('states','states.id','=','students.state_id')
			->where([
			['students.id', $id],
			['students.school_id', $school_id],
			['students.status', '1'],
			])
            ->first();
    	if(count($students)>0){
			return $students;
		}
		else{
			abort(404);
		}
	}
	public function editStudent($id)
	{
		$school_id = Auth::user()->school_id;
		$students = DB::table('students')
			->where([
			['id', $id],
			['school_id', $school_id],
			['status', '1'],
			])
            ->first();
    	if(count($students)>0){
			return $students;
		}
		else{
			abort(404);
		}
	}
	public function updateStudent($id,$data)
	{
		$user_id = $data['user_id'];
		$school_id = $data['school_id'];
		$name = $data['name'];
		$roll_no = $data['roll_no'];
		$registration_no = $data['registration_no'];
		$registration_date = date_format(date_create($data['registration_date']),"Y-m-d");
		$session = $data['session'];
		$classs = $data['classs'];
		$section = $data['section'];
		$dob = date_format(date_create($data['dob']),"Y-m-d");
		$blood_group = $data['blood_group'];
		$state = $data['state'];
		$city = $data['city'];
		$address = $data['address'];
		$pincode = $data['pincode'];
		$father_name = $data['father_name'];
		$mother_name = $data['mother_name'];
		$parent_email = $data['parent_email'];
		$parent_mobile = $data['parent_mobile'];
		
		return DB::table('students')
            ->where('id', $id)
            ->update(['school_id' => $school_id,'name' => $name,'roll_no' => $roll_no,'registration_no' => $registration_no,'registration_date' => $registration_date,'session_id' => $session,'class_id' => $classs,'section_id' => $section,'dob' => $dob,'blood_group' => $blood_group,'state_id' => $state,'city' => $city,'address' => $address,'pincode' => $pincode,'father_name' => $father_name,'mother_name' => $mother_name,'parent_email' => $parent_email,'parent_mobile' => $parent_mobile,'updated_at' => $this->date,'updated_by' => $user_id]);
            
	}
	public function deleteStudent($id)
	{
		$user_id = Auth::id();
		$school_id = Auth::user()->school_id;
		$students = DB::table('students')
			->where([
			['id', $id],
			['school_id', $school_id],
			['status', '1'],
			])
            ->first();
    	if(count($students)>0){
			return DB::table('students')
	            ->where('id', $id)
	            ->update(['status' => '0','updated_at' => $this->date,'updated_by' => $user_id]);
		}
		else{
			abort(404);
		}
	}
}
