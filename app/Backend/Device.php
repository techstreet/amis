<?php

namespace App\Backend;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Http\Requests;
use Carbon\Carbon;

class Device extends Model
{
	public function __construct()
    {
		$this->date = Carbon::now('Asia/Kolkata');
    }
    public function listDevice()
	{
		$school_id = Auth::user()->school_id;
		return DB::table('devices')
			->where([
			['school_id', $school_id],
			['status', '1'],
			])
			->orderBy('name')
            ->get();
	}
	public function addDevice($data)
	{
		$user_id = $data['user_id'];
		$school_id = $data['school_id'];
		$name = $data['name'];
		$description = $data['description'];
			
		return DB::table('devices')->insert(
		    ['school_id' => $school_id,'name' => $name,'description' => $description,'created_at' => $this->date,'created_by' => $user_id]
		);
	}
	public function viewDevice($id)
	{
		$school_id = Auth::user()->school_id;
		$devices = DB::table('devices')
			->where([
			['id', $id],
			['school_id', $school_id],
			['status', '1'],
			])
            ->first();
    	if(count($devices)>0){
			return $devices;
		}
		else{
			abort(404);
		}
	}
	public function editDevice($id)
	{
		$school_id = Auth::user()->school_id;
		$devices = DB::table('devices')
			->where([
			['id', $id],
			['school_id', $school_id],
			['status', '1'],
			])
            ->first();
        if(count($devices)>0){
			return $devices;
		}
		else{
			abort(404);
		}
	}
	public function updateDevice($id,$data)
	{
		$user_id = $data['user_id'];
		$school_id = $data['school_id'];
		$name = $data['name'];
		$description = $data['description'];
		
		return DB::table('devices')
            ->where('id', $id)
            ->update(['school_id' => $school_id,'name' => $name,'description' => $description,'updated_at' => $this->date,'updated_by' => $user_id]);
            
	}
	public function deleteDevice($id)
	{
		$user_id = Auth::id();
		$school_id = Auth::user()->school_id;
		$devices = DB::table('devices')
			->where([
			['id', $id],
			['school_id', $school_id],
			['status', '1'],
			])
            ->first();
    	if(count($devices)>0){
			return DB::table('devices')
	            ->where('id', $id)
	            ->update(['status' => '0','updated_at' => $this->date,'updated_by' => $user_id]);
		}
		else{
			abort(404);
		}
	}
}
