<?php

namespace App\Backend;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Http\Requests;
use Carbon\Carbon;

class Classs extends Model
{
	public function __construct()
    {
		$this->date = Carbon::now('Asia/Kolkata');
    }
    public function listClasss()
	{
		$school_id = Auth::user()->school_id;
		return DB::table('classes')
			->where([
			['school_id', $school_id],
			['status', '1'],
			])
			->orderBy('name')
            ->get();
	}
	public function addClasss($data)
	{
		$user_id = $data['user_id'];
		$school_id = $data['school_id'];
		$name = $data['name'];
		$description = $data['description'];
		$sections = $data['sections'];
			
		return DB::table('classes')->insert(
		    ['school_id' => $school_id,'name' => $name,'description' => $description,'sections' => $sections,'created_at' => $this->date,'created_by' => $user_id]
		);
	}
	public function viewClasss($id)
	{
		$school_id = Auth::user()->school_id;
		$classes = DB::table('classes')
			->where([
			['id', $id],
			['school_id', $school_id],
			['status', '1'],
			])
            ->first();
    	if(count($classes)>0){
			return $classes;
		}
		else{
			abort(404);
		}
	}
	public function editClasss($id)
	{
		$school_id = Auth::user()->school_id;
		$classes = DB::table('classes')
			->where([
			['id', $id],
			['school_id', $school_id],
			['status', '1'],
			])
            ->first();
    	if(count($classes)>0){
			return $classes;
		}
		else{
			abort(404);
		}
	}
	public function updateClasss($id,$data)
	{
		$user_id = $data['user_id'];
		$school_id = $data['school_id'];
		$name = $data['name'];
		$description = $data['description'];
		$sections = $data['sections'];
		
		return DB::table('classes')
            ->where('id', $id)
            ->update(['school_id' => $school_id,'name' => $name,'description' => $description,'sections' => $sections,'updated_at' => $this->date,'updated_by' => $user_id]);
            
	}
	public function deleteClasss($id)
	{
		$user_id = Auth::id();
		$school_id = Auth::user()->school_id;
		$classes = DB::table('classes')
			->where([
			['id', $id],
			['school_id', $school_id],
			['status', '1'],
			])
            ->first();
    	if(count($classes)>0){
			return DB::table('classes')
	            ->where('id', $id)
	            ->update(['status' => '0','updated_at' => $this->date,'updated_by' => $user_id]);
		}
		else{
			abort(404);
		}
	}
}
