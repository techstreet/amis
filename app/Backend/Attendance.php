<?php

namespace App\Backend;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Http\Requests;
use Carbon\Carbon;

class Attendance extends Model
{
	public function __construct()
    {
		$this->date = Carbon::now('Asia/Kolkata');
    }
    public function listAttendance()
	{
		$school_id = Auth::user()->school_id;
		return DB::table('attendance')
			->select('attendance.*','students.name as student_name','students.roll_no','classes.name as class_name','sections.name as section_name','sessions.name as session_name')
			->leftJoin('students','students.id','=','attendance.student_id')
			->leftJoin('classes','students.class_id','=','classes.id')
			->leftJoin('sections','students.section_id','=','sections.id')
			->leftJoin('sessions','students.session_id','=','sessions.id')
			->where([
			['attendance.school_id', $school_id],
			])
			->orderBy('attendance.created_at','desc')
            ->get();
	}
	public function viewAttendance($id)
	{
		$school_id = Auth::user()->school_id;
		$attendances = DB::table('attendance')
			->select('attendance.*','students.name as student_name','students.roll_no','classes.name as class_name','sections.name as section_name','sessions.name as session_name')
			->leftJoin('students','students.id','=','attendance.student_id')
			->leftJoin('classes','students.class_id','=','classes.id')
			->leftJoin('sections','students.section_id','=','sections.id')
			->leftJoin('sessions','students.session_id','=','sessions.id')
			->where([
			['attendance.id', $id],
			['attendance.school_id', $school_id],
			])
            ->first();
        if(count($attendances)>0){
			return $attendances;
		}
		else{
			abort(404);
		}
	}
	public function cancelAttendance($id)
	{
		$user_id = Auth::id();
		$school_id = Auth::user()->school_id;
		$attendances = DB::table('attendance')
			->where([
			['id', $id],
			['school_id', $school_id],
			['status', '1'],
			])
            ->first();
        if(count($attendances)>0){
			return DB::table('attendance')
	            ->where('id', $id)
	            ->update(['status' => '0','updated_at' => $this->date,'updated_by' => $user_id]);
		}
		else{
			abort(404);
		}
	}
	public function activeAttendance($id)
	{
		$user_id = Auth::id();
		$school_id = Auth::user()->school_id;
		$attendances = DB::table('attendance')
			->where([
			['id', $id],
			['school_id', $school_id],
			['status', '0'],
			])
            ->first();
        if(count($attendances)>0){
			return DB::table('attendance')
	            ->where('id', $id)
	            ->update(['status' => '1','updated_at' => $this->date,'updated_by' => $user_id]);
		}
		else{
			abort(404);
		}
	}
}
