<?php

namespace App\Backend;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Http\Requests;
use Carbon\Carbon;

class Session extends Model
{
	public function __construct()
    {
		$this->date = Carbon::now('Asia/Kolkata');
    }
    public function listSession()
	{
		return DB::table('sessions')
			->where([
			['status', '1'],
			])
			->orderBy('name')
            ->get();
	}
	public function addSession($data)
	{
		$user_id = $data['user_id'];
		$name = $data['name'];
		$description = $data['description'];
			
		return DB::table('sessions')->insert(
		    ['name' => $name,'description' => $description,'created_at' => $this->date,'created_by' => $user_id]
		);
	}
	public function viewSession($id)
	{
		$sessions = DB::table('sessions')
			->where([
			['id', $id],
			['status', '1'],
			])
            ->first();
        if(count($sessions)>0){
			return $sessions;
		}
		else{
			abort(404);
		}
	}
	public function editSession($id)
	{
		$sessions = DB::table('sessions')
			->where([
			['id', $id],
			['status', '1'],
			])
            ->first();
        if(count($sessions)>0){
			return $sessions;
		}
		else{
			abort(404);
		}
	}
	public function updateSession($id,$data)
	{
		$user_id = $data['user_id'];
		$name = $data['name'];
		$description = $data['description'];
		
		return DB::table('sessions')
            ->where('id', $id)
            ->update(['name' => $name,'description' => $description,'updated_at' => $this->date,'updated_by' => $user_id]);
            
	}
	public function deleteSession($id)
	{
		$user_id = Auth::id();
		$sessions = DB::table('sessions')
			->where([
			['id', $id],
			['status', '1'],
			])
            ->first();
        if(count($sessions)>0){
			return DB::table('sessions')
	            ->where('id', $id)
	            ->update(['status' => '0','updated_at' => $this->date,'updated_by' => $user_id]);
		}
		else{
			abort(404);
		}
	}
}
