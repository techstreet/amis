@extends('layouts.backend')
@section('content')
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
    	<div class="page-bar">
            <ul class="page-breadcrumb breadcrumb">
                <li>
                    <a href="{{url('/')}}"><i class="icon-home"></i> Home</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="{{url('/sessions')}}">Sessions</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <span class="active">{{$session->name}}</span>
                </li>
            </ul>
        </div>
		<div class="row">
            <div class="col-md-12">
                <div class="portlet light bordered">
                    <div class="portlet-title" style="margin-bottom: 0px">
		                <div class="caption">
		                    <span class="caption-subject font-blue-sharp bold uppercase">{{$session->name}}</span>
		                </div>
		            </div>
                    <div class="portlet-body">
                        <div>
                        	<div class="row">
                        		<div class="col-md-3 col-sm-3 col-xs-12">
                        			<h4 class="profile-desc-title">Description</h4>
                           			@if(!empty($session->description))
                           			<span class="profile-desc-text">{{$session->description}}</span>
                           			@else
                           			<span class="profile-desc-text">Not Provided</span>
                           			@endif
                        		</div>
                        	</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@endsection