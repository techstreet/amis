@extends('layouts.backend')
@section('content')
<?php
//$sections = explode(",",$classs->sections);
?>
<script src="{{asset('backend/assets/pages/scripts/modernizr.custom.js')}}" type="text/javascript"></script>
<style>
.my-toggle-class {
color: #888;
cursor: pointer;
font-size: 0.75em;
font-weight: bold;
padding: 0.5em 1em;
text-transform: uppercase;
}
</style>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
		<div class="page-bar">
            <ul class="page-breadcrumb breadcrumb">
                <li>
                    <a href="{{url('/')}}"><i class="icon-home"></i> Home</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="{{url('/classes')}}">Classes</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="{{url('/classes/view/'.$classs->id)}}">{{$classs->name}}</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <span class="active">Edit</span>
                </li>
            </ul>
        </div>
		<div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <span class="caption-subject font-blue-sharp bold uppercase">Edit Classs</span>
                </div>
            </div>
            <div class="portlet-body">
                <div class="row">
                	<div class="col-md-6">
                		@include('backend/flashmessage')
                		<form method="post" action="">
	                        <div class="form-body">
	                            {{ csrf_field() }}
							    <div class="form-group">
							      <label>*Name:</label>
							      <input type="text" class="form-control" name="name" id="name" value="{{ $classs->name }}" placeholder="Please Enter Name" required="">
							    </div>
							    <div class="form-group">
							      <label>Description:</label>
							      <input type="text" class="form-control" name="description" id="description" value="{{ $classs->description }}" placeholder="Please Enter Description">
							    </div>
							    <div class="form-group">
							      <label>Sections:</label>
							      <div class="mt-checkbox-list mt-checkbox-inline">
							      @foreach($sections as $value)
                                    <label class="mt-checkbox mt-checkbox-outline">{{$value->name}}
	                                    <?php
	                                    $checked = '';
	                                    if(!empty($classs->sections)){
											$section_arr = explode(",",$classs->sections);
											$section_count = count($section_arr);
											if($section_count>0){
												for($i=0; $i<$section_count; $i++){
													if($section_arr[$i] == $value->id){
														$checked = 'checked';
													}
												}
											}
										}
	                                    ?>
                                        <input type="checkbox" value="{{$value->id}}" name="sections[]" {{$checked}}>
                                        <span></span>
                                    </label>
                                  @endforeach
                                  </div>
							    </div>
	                        </div>
	                        <div class="form-actions">
	                            <button type="submit" class="btn blue">Update</button>
	                            <button type="button" class="btn default" onclick="location.href = '{{url('/classes')}}';">Cancel</button>
	                        </div>
	                    </form>
                	</div>
                </div>
            </div>
	    </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
<!-- Include the plugin. Yay! --> 
<script src="{{asset('backend/assets/pages/scripts/hideShowPassword.min.js')}}" type="text/javascript"></script>
<script>
// Example 2
$('#old_password,#password,#confirm_password').hideShowPassword({
  // Make the password visible right away.
  show: false,
  // Create the toggle goodness.
  innerToggle: true,
  // Give the toggle a custom class so we can style it
  // separately from the previous example.
  toggleClass: 'my-toggle-class',
  // Don't show the toggle until the input triggers
  // the 'focus' event.
  hideToggleUntil: 'focus',
  // Enable touch support for toggle.
  touchSupport: Modernizr.touch
});
</script>
@endsection