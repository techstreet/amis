@extends('layouts.backend')
@section('content')
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
    	<div class="page-bar">
            <ul class="page-breadcrumb breadcrumb">
                <li>
                    <a href="{{url('/')}}"><i class="icon-home"></i> Home</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="{{url('/classes')}}">Classes</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <span class="active">{{$classs->name}}</span>
                </li>
            </ul>
        </div>
		<div class="row">
            <div class="col-md-12">
                <div class="portlet light bordered">
                    <div class="portlet-title" style="margin-bottom: 0px">
		                <div class="caption">
		                    <span class="caption-subject font-blue-sharp bold uppercase">{{$classs->name}}</span>
		                </div>
		            </div>
                    <div class="portlet-body">
                        <div>
                        	<div class="row">
                        		<div class="col-md-3 col-sm-3 col-xs-12">
                        			<h4 class="profile-desc-title">Description</h4>
                           			@if(!empty($classs->description))
                           			<span class="profile-desc-text">{{$classs->description}}</span>
                           			@else
                           			<span class="profile-desc-text">Not Provided</span>
                           			@endif
                        		</div>
                        		<div class="col-md-3 col-sm-3 col-xs-12">
                        			<h4 class="profile-desc-title">Sections</h4>
                        			@if(!empty($classs->sections))
                           			<span class="profile-desc-text">
                           			<?php
                                    if(!empty($classs->sections)){
										$section_arr = explode(",",$classs->sections);
										$section_count = count($section_arr);
										if($section_count>0){
											for($i=0; $i<$section_count; $i++){
												echo '<span style="margin-right:5px" class="badge">'.getSectionName($section_arr[$i]).'</span>';
											}
										}
									}
                                    ?>
                           			</span>
                           			@else
                           			<span class="profile-desc-text">Not Provided</span>
                           			@endif
                        		</div>
                        	</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@endsection