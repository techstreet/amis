@extends('layouts.backend')
@section('content')
<script src="{{asset('backend/assets/pages/scripts/modernizr.custom.js')}}" type="text/javascript"></script>
<style>
.my-toggle-class {
color: #888;
cursor: pointer;
font-size: 0.75em;
font-weight: bold;
padding: 0.5em 1em;
text-transform: uppercase;
}
</style>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
		<div class="page-bar">
            <ul class="page-breadcrumb breadcrumb">
                <li>
                    <a href="{{url('/')}}"><i class="icon-home"></i> Home</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="{{url('/schools')}}">Schools</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="{{url('/schools/view/'.$school->id)}}">{{$school->name}}</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <span class="active">Edit</span>
                </li>
            </ul>
        </div>
		<div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <span class="caption-subject font-blue-sharp bold uppercase">Edit School</span>
                </div>
            </div>
            <div class="portlet-body">
                <div class="row">
                	<div class="col-md-6">
                		@include('backend/flashmessage')
                		<form method="post" action="">
	                        <div class="form-body">
	                            {{ csrf_field() }}
							    <div class="form-group">
							      <label>*Name:</label>
							      <input type="text" class="form-control" name="name" id="name" value="{{ $school->name }}" placeholder="Please Enter Name" required="">
							    </div>
							    <div class="form-group">
							      <label>*Email:</label>
							      <input type="email" class="form-control" name="email" id="email" value="{{ $school->email }}" placeholder="Please Enter Email" required="">
							    </div>
							    <div class="form-group">
							      <label>*Phone:</label>
							      <input type="text" class="form-control" name="phone" id="phone" value="{{ $school->phone }}" placeholder="Please Enter Phone" data-role="tagsinput">
							    </div>
							    <div class="form-group">
							      <label>Mobile:</label>
							      <input type="text" class="form-control" name="mobile" id="mobile" value="{{ $school->mobile }}" placeholder="Please Enter Mobile" data-role="tagsinput">
							    </div>
							    <div class="form-group">
							      <label>*Website:</label>
							      <input type="text" class="form-control" name="website" id="website" value="{{ $school->website }}" placeholder="Please Enter Website" required="">
							    </div>
							    <div class="form-group">
							      <label>*State:</label>
							      <select name="state" class="form-control" required="">
							      	@foreach($states as $value)
							      	<option value="{{$value->id}}" @if($school->state_id == $value->id) selected @endif>{{$value->name}}</option>
							      	@endforeach
							      </select>
							    </div>
							    <div class="form-group">
							      <label>*City:</label>
							      <input type="text" class="form-control" name="city" id="city" value="{{ $school->city }}" placeholder="Please Enter City" required="">
							    </div>
							    <div class="form-group">
							      <label>*Address:</label>
							      <textarea class="form-control" name="address" id="address" placeholder="Please Enter Address" required="">{{ $school->address }}</textarea>
							    </div>
	                        </div>
	                        <div class="form-actions">
	                            <button type="submit" class="btn blue">Update</button>
	                            <button type="button" class="btn default" onclick="location.href = '{{url('/schools')}}';">Cancel</button>
	                        </div>
	                    </form>
                	</div>
                </div>
            </div>
	    </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
<!-- Include the plugin. Yay! --> 
<script src="{{asset('backend/assets/pages/scripts/hideShowPassword.min.js')}}" type="text/javascript"></script>
<script>
// Example 2
$('#old_password,#password,#confirm_password').hideShowPassword({
  // Make the password visible right away.
  show: false,
  // Create the toggle goodness.
  innerToggle: true,
  // Give the toggle a custom class so we can style it
  // separately from the previous example.
  toggleClass: 'my-toggle-class',
  // Don't show the toggle until the input triggers
  // the 'focus' event.
  hideToggleUntil: 'focus',
  // Enable touch support for toggle.
  touchSupport: Modernizr.touch
});
</script>
@endsection