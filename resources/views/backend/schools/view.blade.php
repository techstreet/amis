@extends('layouts.backend')
@section('content')
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
    	<div class="page-bar">
            <ul class="page-breadcrumb breadcrumb">
                <li>
                    <a href="{{url('/')}}"><i class="icon-home"></i> Home</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="{{url('/schools')}}">Schools</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <span class="active">{{$school->name}}</span>
                </li>
            </ul>
        </div>
		<div class="row">
            <div class="col-md-12">
                <div class="portlet light bordered">
                    <div class="portlet-title" style="margin-bottom: 0px">
		                <div class="caption">
		                    <span class="caption-subject font-blue-sharp bold uppercase">{{$school->name}}</span>
		                </div>
		            </div>
                    <div class="portlet-body">
                        <div class="row list-separated profile-stat">
                        	<div class="col-md-4">
                        		<div class="row">
                        			<div class="col-md-4 col-sm-4 col-xs-4">
		                                <div class="uppercase profile-stat-title"> 37 </div>
		                                <div class="uppercase profile-stat-text"> Students </div>
		                            </div>
		                            <div class="col-md-4 col-sm-4 col-xs-4">
		                                <div class="uppercase profile-stat-title"> 51 </div>
		                                <div class="uppercase profile-stat-text"> Classes </div>
		                            </div>
		                            <div class="col-md-4 col-sm-4 col-xs-4">
		                                <div class="uppercase profile-stat-title"> 61 </div>
		                                <div class="uppercase profile-stat-text"> Sections </div>
		                            </div>
                        		</div>
                        	</div>
                        </div>
                        <div>
                        	<div class="row">
                        		<div class="col-md-3 col-sm-3 col-xs-12">
                        			<h4 class="profile-desc-title">Email</h4>
                           			@if(!empty($school->email))
                           			<span class="profile-desc-text">{{$school->email}}</span>
                           			@else
                           			<span class="profile-desc-text">Not Provided</span>
                           			@endif
                        		</div>
                        		<div class="col-md-3 col-sm-3 col-xs-12">
                        			<h4 class="profile-desc-title">Phone</h4>
                           			@if(!empty($school->phone))
                           			<span class="profile-desc-text">{{$school->phone}}</span>
                           			@else
                           			<span class="profile-desc-text">Not Provided</span>
                           			@endif
                        		</div>
                        		<div class="col-md-3 col-sm-3 col-xs-12">
                        			<h4 class="profile-desc-title">Mobile</h4>
                        			@if(!empty($school->mobile))
                           			<span class="profile-desc-text">{{$school->mobile}}</span>
                           			@else
                           			<span class="profile-desc-text">Not Provided</span>
                           			@endif
                        		</div>
                        		<div class="col-md-3 col-sm-3 col-xs-12">
                        			<h4 class="profile-desc-title">Website</h4>
                           			@if(!empty($school->website))
                           			<span class="profile-desc-text">{{$school->website}}</span>
                           			@else
                           			<span class="profile-desc-text">Not Provided</span>
                           			@endif
                        		</div>
                        	</div>
                        	<div class="row">
                        		<div class="col-md-3 col-sm-3 col-xs-12">
                        			<h4 class="profile-desc-title">State</h4>
                           			@if(!empty($school->state))
                           			<span class="profile-desc-text">{{$school->state}}</span>
                           			@else
                           			<span class="profile-desc-text">Not Provided</span>
                           			@endif
                        		</div>
                        		<div class="col-md-3 col-sm-3 col-xs-12">
                        			<h4 class="profile-desc-title">City</h4>
                           			@if(!empty($school->city))
                           			<span class="profile-desc-text">{{$school->city}}</span>
                           			@else
                           			<span class="profile-desc-text">Not Provided</span>
                           			@endif
                        		</div>
                        		<div class="col-md-3 col-sm-3 col-xs-12">
                        			<h4 class="profile-desc-title">Address</h4>
                           			@if(!empty($school->address))
                           			<span class="profile-desc-text">{{$school->address}}</span>
                           			@else
                           			<span class="profile-desc-text">Not Provided</span>
                           			@endif
                        		</div>
                        	</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@endsection