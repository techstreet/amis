@extends('layouts.backend')
@section('content')
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
    	<div class="page-bar">
            <ul class="page-breadcrumb breadcrumb">
                <li>
                    <a href="{{url('/')}}"><i class="icon-home"></i> Home</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="{{url('/students')}}">Students</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <span class="active">{{$student->name}}</span>
                </li>
            </ul>
        </div>
		<div class="row">
            <div class="col-md-12">
                <div class="portlet light bordered">
                    <div class="portlet-title" style="margin-bottom: 0px">
		                <div class="caption">
		                    <span class="caption-subject font-blue-sharp bold uppercase">{{$student->name}}</span>
		                </div>
		            </div>
                    <div class="portlet-body">
                        <div>
                        	<div class="row">
                        		<div class="col-md-3 col-sm-3 col-xs-12">
                        			<h4 class="profile-desc-title">Roll No</h4>
                           			@if(!empty($student->roll_no))
                           			<span class="profile-desc-text">{{$student->roll_no}}</span>
                           			@else
                           			<span class="profile-desc-text">Not Provided</span>
                           			@endif
                        		</div>
                        		<div class="col-md-3 col-sm-3 col-xs-12">
                        			<h4 class="profile-desc-title">Registration No</h4>
                           			@if(!empty($student->registration_no))
                           			<span class="profile-desc-text">{{$student->registration_no}}</span>
                           			@else
                           			<span class="profile-desc-text">Not Provided</span>
                           			@endif
                        		</div>
                        		<div class="col-md-3 col-sm-3 col-xs-12">
                        			<h4 class="profile-desc-title">Registration Date</h4>
                           			@if(!empty($student->registration_date))
                           			<span class="profile-desc-text">{{date_dfy($student->registration_date)}}</span>
                           			@else
                           			<span class="profile-desc-text">Not Provided</span>
                           			@endif
                        		</div>
                        		<div class="col-md-3 col-sm-3 col-xs-12">
                        			<h4 class="profile-desc-title">Session</h4>
                           			@if(!empty($student->session_name))
                           			<span class="profile-desc-text">{{$student->session_name}}</span>
                           			@else
                           			<span class="profile-desc-text">Not Provided</span>
                           			@endif
                        		</div>
                        		
                        	</div>
							<div class="row">
								<div class="col-md-3 col-sm-3 col-xs-12">
                        			<h4 class="profile-desc-title">Class</h4>
                           			@if(!empty($student->class_name))
                           			<span class="profile-desc-text">{{$student->class_name}}</span>
                           			@else
                           			<span class="profile-desc-text">Not Provided</span>
                           			@endif
                        		</div>
                        		<div class="col-md-3 col-sm-3 col-xs-12">
                        			<h4 class="profile-desc-title">Section</h4>
                           			@if(!empty($student->section_name))
                           			<span class="profile-desc-text">{{$student->section_name}}</span>
                           			@else
                           			<span class="profile-desc-text">Not Provided</span>
                           			@endif
                        		</div>
                        		<div class="col-md-3 col-sm-3 col-xs-12">
                        			<h4 class="profile-desc-title">Date of Birth</h4>
                           			@if(!empty($student->dob))
                           			<span class="profile-desc-text">{{date_dfy($student->dob)}}</span>
                           			@else
                           			<span class="profile-desc-text">Not Provided</span>
                           			@endif
                        		</div>
                        		<div class="col-md-3 col-sm-3 col-xs-12">
                        			<h4 class="profile-desc-title">Blood Group</h4>
                           			@if(!empty($student->blood_group))
                           			<span class="profile-desc-text">{{$student->blood_group}}</span>
                           			@else
                           			<span class="profile-desc-text">Not Provided</span>
                           			@endif
                        		</div>
                        	</div>
							<div class="row">
                        		<div class="col-md-3 col-sm-3 col-xs-12">
                        			<h4 class="profile-desc-title">State</h4>
                           			@if(!empty($student->state_name))
                           			<span class="profile-desc-text">{{$student->state_name}}</span>
                           			@else
                           			<span class="profile-desc-text">Not Provided</span>
                           			@endif
                        		</div>
                        		<div class="col-md-3 col-sm-3 col-xs-12">
                        			<h4 class="profile-desc-title">City</h4>
                           			@if(!empty($student->city))
                           			<span class="profile-desc-text">{{$student->city}}</span>
                           			@else
                           			<span class="profile-desc-text">Not Provided</span>
                           			@endif
                        		</div>
                        		<div class="col-md-3 col-sm-3 col-xs-12">
                        			<h4 class="profile-desc-title">Address</h4>
                           			@if(!empty($student->address))
                           			<span class="profile-desc-text">{{$student->address}}</span>
                           			@else
                           			<span class="profile-desc-text">Not Provided</span>
                           			@endif
                        		</div>
                        		<div class="col-md-3 col-sm-3 col-xs-12">
                        			<h4 class="profile-desc-title">Pincode</h4>
                           			@if(!empty($student->pincode))
                           			<span class="profile-desc-text">{{$student->pincode}}</span>
                           			@else
                           			<span class="profile-desc-text">Not Provided</span>
                           			@endif
                        		</div>
                        	</div>
							<div class="row">
                        		<div class="col-md-3 col-sm-3 col-xs-12">
                        			<h4 class="profile-desc-title">Father's Name</h4>
                           			@if(!empty($student->father_name))
                           			<span class="profile-desc-text">{{$student->father_name}}</span>
                           			@else
                           			<span class="profile-desc-text">Not Provided</span>
                           			@endif
                        		</div>
                        		<div class="col-md-3 col-sm-3 col-xs-12">
                        			<h4 class="profile-desc-title">Mother's Name</h4>
                           			@if(!empty($student->mother_name))
                           			<span class="profile-desc-text">{{$student->mother_name}}</span>
                           			@else
                           			<span class="profile-desc-text">Not Provided</span>
                           			@endif
                        		</div>
                        		<div class="col-md-3 col-sm-3 col-xs-12">
                        			<h4 class="profile-desc-title">Parent's Email</h4>
                           			@if(!empty($student->parent_email))
                           			<span class="profile-desc-text">{{$student->parent_email}}</span>
                           			@else
                           			<span class="profile-desc-text">Not Provided</span>
                           			@endif
                        		</div>
                        		<div class="col-md-3 col-sm-3 col-xs-12">
                        			<h4 class="profile-desc-title">Parent's Mobile</h4>
                           			@if(!empty($student->parent_mobile))
                           			<span class="profile-desc-text">{{$student->parent_mobile}}</span>
                           			@else
                           			<span class="profile-desc-text">Not Provided</span>
                           			@endif
                        		</div>
                        	</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@endsection