@extends('layouts.backend')
@section('content')
<script type='text/javascript'>
$(function(){
	var nowDate = new Date();
	var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0); 
	$('.input-group.date').datepicker({
	    calendarWeeks: true,
	    todayHighlight: true,
	    autoclose: true,
	    format: "dd-MM-yyyy",
	    //startDate: today
	});
});
</script>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
		<div class="page-bar">
            <ul class="page-breadcrumb breadcrumb">
                <li>
                    <a href="{{url('/')}}"><i class="icon-home"></i> Home</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="{{url('/students')}}">Students</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="{{url('/students/view/'.$student->id)}}">{{$student->name}}</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <span class="active">Edit</span>
                </li>
            </ul>
        </div>
		<div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <span class="caption-subject font-blue-sharp bold uppercase">Edit Student</span>
                </div>
            </div>
            <div class="portlet-body">
                <div class="row">
                	<div class="col-md-12">
                		@include('backend/flashmessage')
                		<form method="post" action="">
	                        <div class="form-body">
	                            {{ csrf_field() }}
							    <div class="row">
	                            	<div class="col-md-6">
	                            		<div class="form-group">
									      <label>*Name:</label>
									      <input type="text" class="form-control" name="name" id="name" value="{{ $student->name }}" placeholder="Please Enter Name" required="">
									    </div>
	                            	</div>
	                            	<div class="col-md-6">
	                            		<div class="form-group">
									      <label>*Roll No:</label>
									      <input type="number" class="form-control" name="roll_no" id="roll_no" value="{{ $student->roll_no }}" placeholder="Please Enter Roll No" required="">
									    </div>
	                            	</div>
	                            </div>
	                            <div class="row">
	                            	<div class="col-md-6">
	                            		<div class="form-group">
									      <label>*Registration No:</label>
									      <input type="text" class="form-control" name="registration_no" id="registration_no" value="{{ $student->registration_no }}" placeholder="Please Enter Registration No" required="">
									    </div>
	                            	</div>
	                            	<div class="col-md-6">
	                            		<div class="form-group">
									      <label>*Registration Date:</label>
									      <div class="input-group date">
											<input type="text" name="registration_date" id="registration_date" value="{{ date_dfy($student->registration_date) }}" placeholder="Please Enter Registration Date" class="form-control" required=""><span class="input-group-addon"><i class="fa fa-calendar"></i></span>
										  </div>
									    </div>
	                            	</div>
	                            </div>
	                            <div class="row">
	                            	<div class="col-md-6">
	                            		<div class="form-group">
									      <label>*Session:</label>
									      <select name="session" id="session" class="form-control">
									      	@foreach($sessions as $value)
									      	<option value="{{$value->id}}" @if($student->session_id == $value->id) selected @endif>{{$value->name}}</option>
									      	@endforeach
									      </select>
									    </div>
	                            	</div>
	                            	<div class="col-md-6">
	                            		<div class="form-group">
									      <label>*Class:</label>
									      <select name="classs" id="classs" class="form-control">
									      	@foreach($classes as $value)
									      	<option value="{{$value->id}}"@if($student->class_id == $value->id) selected @endif>{{$value->name}}</option>
									      	@endforeach
									      </select>
									    </div>
	                            	</div>
	                            </div>
	                            <div class="row">
	                            	<div class="col-md-6">
	                            		<div class="form-group">
									      <label>*Section:</label>
									      <select name="section" id="section" class="form-control">
									      	@foreach($sections as $value)
									      	<option value="{{$value->id}}"@if($student->section_id == $value->id) selected @endif>{{$value->name}}</option>
									      	@endforeach
									      </select>
									    </div>
	                            	</div>
	                            	<div class="col-md-6">
	                            		<div class="form-group">
									      <label>*Date of Birth:</label>
									      <div class="input-group date">
											<input type="text" name="dob" id="dob" value="{{ date_dfy($student->dob) }}" placeholder="Please Enter Date of Birth" class="form-control" required=""><span class="input-group-addon"><i class="fa fa-calendar"></i></span>
										  </div>
									    </div>
	                            	</div>
	                            </div>
	                            <div class="row">
	                            	<div class="col-md-6">
	                            		<div class="form-group">
									      <label>*Blood Group:</label>
									      <select name="blood_group" id="blood_group" class="form-control">
									      	<option>Select</option>
									      	<option value="A+"@if($student->blood_group == 'A+') selected @endif>A+</option>
									      	<option value="A-"@if($student->blood_group == 'A-') selected @endif>A-</option>
									      	<option value="B+"@if($student->blood_group == 'B+') selected @endif>B+</option>
									      	<option value="B-"@if($student->blood_group == 'B-') selected @endif>B-</option>
									      	<option value="AB+"@if($student->blood_group == 'AB+') selected @endif>AB+</option>
									      	<option value="AB-"@if($student->blood_group == 'AB-') selected @endif>AB-</option>
									      	<option value="O+"@if($student->blood_group == 'O+') selected @endif>O+</option>
									      	<option value="O-"@if($student->blood_group == 'O-') selected @endif>O-</option>
									      </select>
									    </div>
	                            	</div>
	                            	<div class="col-md-6">
	                            		<div class="form-group">
									      <label>*State:</label>
									      <select name="state" id="state" class="form-control">
									      	@foreach($states as $value)
									      	<option value="{{$value->id}}"@if($student->state_id == $value->id) selected @endif>{{$value->name}}</option>
									      	@endforeach
									      </select>
									    </div>
	                            	</div>
	                            </div>
	                            <div class="row">
	                            	<div class="col-md-6">
	                            		<div class="form-group">
									      <label>*City:</label>
									      <input type="text" class="form-control" name="city" id="city" value="{{ $student->city }}" placeholder="Please Enter City" required="">
									    </div>
	                            	</div>
	                            	<div class="col-md-6">
	                            		<div class="form-group">
									      <label>*Address:</label>
									      <textarea class="form-control" name="address" id="address" placeholder="Please Enter Address" style="height: 34px" required="">{{ $student->address }}</textarea>
									    </div>
	                            	</div>
	                            </div>
	                            <div class="row">
	                            	<div class="col-md-6">
	                            		<div class="form-group">
									      <label>*Pincode:</label>
									      <input type="number" class="form-control" name="pincode" id="city" value="{{ $student->pincode }}" placeholder="Please Enter Pincode" required="">
									    </div>
	                            	</div>
	                            	<div class="col-md-6">
	                            		<div class="form-group">
									      <label>*Father's Name:</label>
									      <input type="text" class="form-control" name="father_name" id="father_name" value="{{ $student->father_name }}" placeholder="Please Enter Father's Name" required="">
									    </div>
	                            	</div>
	                            </div>
	                            <div class="row">
	                            	<div class="col-md-6">
	                            		<div class="form-group">
									      <label>*Mother's Name:</label>
									      <input type="text" class="form-control" name="mother_name" id="mother_name" value="{{ $student->mother_name }}" placeholder="Please Enter Mother's Name" required="">
									    </div>
	                            	</div>
	                            	<div class="col-md-6">
	                            		<div class="form-group">
									      <label>*Parent's Email:</label>
									      <input type="email" class="form-control" name="parent_email" id="parent_email" value="{{ $student->parent_email }}" placeholder="Please Enter Parent's Email" required="">
									    </div>
	                            	</div>
	                            </div>
	                            <div class="row">
	                            	<div class="col-md-6">
	                            		<div class="form-group">
									      <label>*Parent's Mobile:</label>
									      <input type="text" class="form-control" name="parent_mobile" id="parent_mobile" value="{{ $student->parent_mobile }}" placeholder="Please Enter Parent's Mobile" maxlength="10" required="">
									    </div>
	                            	</div>
	                            </div> 
	                        </div>
	                        <div class="form-actions">
	                            <button type="submit" class="btn blue">Update</button>
	                            <button type="button" class="btn default" onclick="location.href = '{{url('/students')}}';">Cancel</button>
	                        </div>
	                    </form>
                	</div>
                </div>
            </div>
	    </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@endsection