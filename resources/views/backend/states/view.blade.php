@extends('layouts.backend')
@section('content')
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
    	<div class="page-bar">
            <ul class="page-breadcrumb breadcrumb">
                <li>
                    <a href="{{url('/')}}"><i class="icon-home"></i> Home</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="{{url('/states')}}">States</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <span class="active">{{$state->name}}</span>
                </li>
            </ul>
        </div>
		<div class="row">
            <div class="col-md-12">
                <div class="portlet light bordered">
                    <div class="portlet-title" style="margin-bottom: 0px">
		                <div class="caption">
		                    <span class="caption-subject font-blue-sharp bold uppercase">{{$state->name}}</span>
		                </div>
		            </div>
                    <div class="portlet-body">
                        <div>
                        	<div class="row">
                        		<div class="col-md-3 col-sm-3 col-xs-12">
                        			<h4 class="profile-desc-title">Short Name</h4>
                           			@if(!empty($state->short_name))
                           			<span class="profile-desc-text">{{$state->short_name}}</span>
                           			@else
                           			<span class="profile-desc-text">Not Provided</span>
                           			@endif
                        		</div>
                        		<div class="col-md-3 col-sm-3 col-xs-12">
                        			<h4 class="profile-desc-title">Sort Order</h4>
                        			@if(!empty($state->sort_order))
                           			<span class="profile-desc-text">{{$state->sort_order}}</span>
                           			@else
                           			<span class="profile-desc-text">Not Provided</span>
                           			@endif
                        		</div>
                        	</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@endsection