@extends('layouts.backend')
@section('content')
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
    	<div class="page-bar">
            <ul class="page-breadcrumb breadcrumb">
                <li>
                    <a href="{{url('/')}}"><i class="icon-home"></i> Home</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="{{url('/attendance')}}">Attendance</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="{{url('/students/view/'.$attendance->student_id)}}">{{$attendance->student_name}}</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <span class="active">{{date_dfy($attendance->created_at)}}</span>
                </li>
            </ul>
        </div>
		<div class="row">
            <div class="col-md-12">
                <div class="portlet light bordered">
                    <div class="portlet-title" style="margin-bottom: 0px">
		                <div class="caption">
		                    <span class="caption-subject font-blue-sharp bold uppercase">{{$attendance->student_name}}</span>
		                </div>
		            </div>
                    <div class="portlet-body">
                        <div>
                        	<div class="row">
                        		<div class="col-md-3 col-sm-3 col-xs-12">
                        			<h4 class="profile-desc-title">In Time</h4>
                           			@if(!empty($attendance->in_time))
                           			<span class="profile-desc-text">{{am_pm($attendance->in_time)}}</span>
                           			@else
                           			<span class="profile-desc-text">Not Provided</span>
                           			@endif
                        		</div>
                        		<div class="col-md-3 col-sm-3 col-xs-12">
                        			<h4 class="profile-desc-title">Out Time</h4>
                           			@if(!empty($attendance->in_time))
                           			<span class="profile-desc-text">{{am_pm($attendance->out_time)}}</span>
                           			@else
                           			<span class="profile-desc-text">Not Provided</span>
                           			@endif
                        		</div>
                        	</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@endsection