@extends('layouts.backend')
@section('content')
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
    	<div class="page-bar">
            <ul class="page-breadcrumb breadcrumb">
                <li>
                    <a href="{{url('/')}}"><i class="icon-home"></i> Home</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <span class="active">Attendance</span>
                </li>
            </ul>
        </div>
		<div class="row">
            <div class="col-md-12">
                <div class="portlet light bordered">
                    <div class="portlet-title">
		                <div class="caption">
		                    <span class="caption-subject font-blue-sharp bold uppercase">Attendance</span> <!--({{ date_dfy($attendance_date) }})-->
		                </div>
		            </div>
		            @include('backend/flashmessage')
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover" id="dataTable">
                            <thead>
                                <tr>
                                    <th>S.No</th>
                                    <th>Name</th>
                                    <th>Image</th>
                                    <!--<th>Roll No</th>
                                    <th>Class</th>
                                    <th>Section</th>
                                    <th>Session</th>-->
                                    <th>In Time</th>
                                    <th>Out Time</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($attendances as $key=>$value)
                                <tr>
                                    @if($value->status == '0')
                                    <td style="text-decoration: line-through">{{++$key}}</td>
                                    <td style="text-decoration: line-through">{{$value->student_name}}</td>
                                    <td style="text-decoration: line-through">{{$value->image}}</td>
                                   <!-- <td style="text-decoration: line-through">{{$value->roll_no}}</td>
                                    <td style="text-decoration: line-through">{{$value->class_name}}</td>
                                    <td style="text-decoration: line-through">{{$value->section_name}}</td>
                                    <td style="text-decoration: line-through">{{$value->session_name}}</td>-->
                                    <td style="text-decoration: line-through">{{$value->in_time}}</td>
                                    <td style="text-decoration: line-through">{{$value->out_time}}</td>
                                    <td width="150px" style="min-width: 150px">
                                        <a href="{{ url('/attendance/view/'.$value->id) }}" class="btn btn-sm btn-success">
                                            <i class="fa fa-eye"></i> View
                                        </a>
                                        <a onclick="return confirm('Are you sure you want to Undo?');" href="{{ url('/attendance/active/'.$value->id) }}" class="btn btn-sm btn-warning">
                                            <i class="fa fa-undo"></i> Undo
                                        </a>
                                    </td>
                                    @else
                                    <td>{{++$key}}</td>
                                    <td>{{$value->student_name}}</td>
                                    <td>{{$value->image}}</td>
                                    <!--<td>{{$value->roll_no}}</td>
                                    <td>{{$value->class_name}}</td>
                                    <td>{{$value->section_name}}</td>
                                    <td>{{$value->session_name}}</td>-->
                                    <td>{{am_pm($value->in_time)}}</td>
                                    <td>{{am_pm($value->out_time)}}</td>
                                    <td width="150px" style="min-width: 150px">
                                        <a href="{{ url('/attendance/view/'.$value->id) }}" class="btn btn-sm btn-success">
                                            <i class="fa fa-eye"></i> View
                                        </a>
                                        <a onclick="return confirm('Are you sure you want to Cancel?');" href="{{ url('/attendance/cancel/'.$value->id) }}" class="btn btn-sm btn-warning">
                                            <i class="fa fa-times"></i> Cancel
                                        </a>
                                    </td>
                                    @endif
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
<script>
	$(document).ready(function(){
	    $('#dataTable').DataTable({
			"ordering": false
		});
	});
</script>
@endsection