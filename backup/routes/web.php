<?php

/*
|--------------------------------------------------------------------------
| Back-End
|--------------------------------------------------------------------------
*/
Route::group(['namespace' => 'Backend'], function () {
	Route::get('login', [ 'as' => 'login', 'uses' => 'LoginController@index']);
	Route::post('login', [ 'as' => 'login', 'uses' => 'LoginController@login']);
});

Route::group(['namespace' => 'Backend','middleware' => 'auth'], function () {
	//Dashboard
	Route::get('/','WelcomeController@index');
        //Test
	Route::get('/test','TestController@index');
	//Edit Profile
	Route::get('/edit-profile','EditprofileController@index');
	Route::post('/edit-profile','EditprofileController@save');
	//Change password
	Route::get('/change-password','ChangepasswordController@index');
	Route::post('/change-password','ChangepasswordController@save');
	//Logout
	Route::get('/logout','LoginController@logout');
	//States
	Route::get('/states','StateController@index');
	Route::get('/states/add','StateController@add');
	Route::post('/states/add','StateController@save');
	Route::get('/states/edit/{id}','StateController@edit');
	Route::post('/states/edit/{id}','StateController@update');
	Route::get('/states/view/{id}','StateController@view');
	Route::get('/states/delete/{id}','StateController@delete');
	//Schools
	Route::get('/schools','SchoolController@index');
	Route::get('/schools/add','SchoolController@add');
	Route::post('/schools/add','SchoolController@save');
	Route::get('/schools/edit/{id}','SchoolController@edit');
	Route::post('/schools/edit/{id}','SchoolController@update');
	Route::get('/schools/view/{id}','SchoolController@view');
	Route::get('/schools/block/{id}','SchoolController@block');
	Route::get('/schools/unblock/{id}','SchoolController@unblock');
	Route::get('/schools/delete/{id}','SchoolController@delete');
	//Classes
	Route::get('/classes','ClasssController@index');
	Route::get('/classes/add','ClasssController@add');
	Route::post('/classes/add','ClasssController@save');
	Route::get('/classes/edit/{id}','ClasssController@edit');
	Route::post('/classes/edit/{id}','ClasssController@update');
	Route::get('/classes/view/{id}','ClasssController@view');
	Route::get('/classes/delete/{id}','ClasssController@delete');
	//Sections
	Route::get('/sections','SectionController@index');
	Route::get('/sections/add','SectionController@add');
	Route::post('/sections/add','SectionController@save');
	Route::get('/sections/edit/{id}','SectionController@edit');
	Route::post('/sections/edit/{id}','SectionController@update');
	Route::get('/sections/view/{id}','SectionController@view');
	Route::get('/sections/delete/{id}','SectionController@delete');
	//Students
	Route::get('/students','StudentController@index');
	Route::get('/students/add','StudentController@add');
	Route::post('/students/add','StudentController@save');
	Route::get('/students/edit/{id}','StudentController@edit');
	Route::post('/students/edit/{id}','StudentController@update');
	Route::get('/students/view/{id}','StudentController@view');
	Route::get('/students/delete/{id}','StudentController@delete');
});