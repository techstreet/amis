<?php

namespace App\Backend;

use Illuminate\Database\Eloquent\Model;
use DB;
use Hash;
use Auth;
use App\Http\Requests;
use Carbon\Carbon;

class School extends Model
{
	public function __construct()
    {
		$this->date = Carbon::now('Asia/Kolkata');
    }
    public function listSchool()
	{
		return DB::table('schools')
			->select('schools.*','states.name as state','users.is_blocked')
			->leftJoin('users','users.school_id','=','schools.id')
			->leftJoin('states','states.id','=','schools.state_id')
			->where([
			['schools.status', '1'],
			])
            ->get();
	}
	public function addSchool($data)
	{
		$result = DB::transaction(function () use ($data) {
		    $user_id = $data['user_id'];
			$name = $data['name'];
			$email = $data['email'];
			$password = Hash::make($data['password']);
			$phone = $data['phone'];
			$mobile = $data['mobile'];
			$website = $data['website'];
			$state = $data['state'];
			$city = $data['city'];
			$address = $data['address'];
			
			$school_id = DB::table('schools')->insertGetId(
			    ['name' => $name,'email' => $email,'phone' => $phone,'mobile' => $mobile,'website' => $website,'state_id' => $state,'city' => $city,'address' => $address,'created_at' => $this->date,'created_by' => $user_id]
			);
            
            return DB::table('users')->insert(
			    ['school_id' => $school_id,'user_group' => '2','name' => $name,'email' => $email,'mobile' => $mobile,'password' => $password,'created_at' => $this->date]
			);
			
		});
		return TRUE;
	}
	public function viewSchool($id)
	{
		return DB::table('schools')
			->select('schools.*','states.name as state')
			->leftJoin('states','states.id','=','schools.state_id')
			->where([
			['schools.id', $id],
			['schools.status', '1'],
			])
            ->first();
	}
	public function editSchool($id)
	{
		return DB::table('schools')
			->where([
			['id', $id],
			['status', '1'],
			])
            ->first();
	}
	public function updateSchool($id,$data)
	{
		$user_id = $data['user_id'];
		$name = $data['name'];
		$email = $data['email'];
		$phone = $data['phone'];
		$mobile = $data['mobile'];
		$website = $data['website'];
		$state = $data['state'];
		$city = $data['city'];
		$address = $data['address'];
		
		return DB::table('schools')
            ->where('id', $id)
            ->update(['name' => $name,'email' => $email,'phone' => $phone,'mobile' => $mobile,'website' => $website,'state_id' => $state,'city' => $city,'address' => $address,'updated_at' => $this->date,'updated_by' => $user_id]);
            
	}
	public function deleteSchool($id)
	{
		$user_id = Auth::id();
		return DB::table('schools')
            ->where('id', $id)
            ->update(['status' => '0','updated_at' => $this->date,'updated_by' => $user_id]);
	}
	public function blockSchool($id)
	{
		$user_id = Auth::id();
		return DB::table('users')
            ->where('school_id', $id)
            ->where([
			['school_id', $id],
			['is_blocked', '0'],
			])
            ->update(['is_blocked' => '1','updated_at' => $this->date]);
	}
	public function unblockSchool($id)
	{
		$user_id = Auth::id();
		return DB::table('users')
            ->where('school_id', $id)
            ->where([
			['school_id', $id],
			['is_blocked', '1'],
			])
            ->update(['is_blocked' => '0','updated_at' => $this->date]);
	}
}
