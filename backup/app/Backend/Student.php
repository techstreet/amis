<?php

namespace App\Backend;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Http\Requests;
use Carbon\Carbon;

class Student extends Model
{
	public function __construct()
    {
		$this->date = Carbon::now('Asia/Kolkata');
    }
    public function listStudent()
	{
		return DB::table('students')
			->where([
			['status', '1'],
			])
			->orderBy('sort_order')
            ->get();
	}
	public function addStudent($data)
	{
		$user_id = $data['user_id'];
		$name = $data['name'];
		$short_name = $data['short_name'];
		$sort_order = $data['sort_order'];
			
		return DB::table('students')->insert(
		    ['name' => $name,'short_name' => $short_name,'sort_order' => $sort_order,'created_at' => $this->date,'created_by' => $user_id]
		);
	}
	public function viewStudent($id)
	{
		return DB::table('students')
			->where([
			['id', $id],
			['status', '1'],
			])
            ->first();
	}
	public function editStudent($id)
	{
		return DB::table('students')
			->where([
			['id', $id],
			['status', '1'],
			])
            ->first();
	}
	public function updateStudent($id,$data)
	{
		$user_id = $data['user_id'];
		$name = $data['name'];
		$short_name = $data['short_name'];
		$sort_order = $data['sort_order'];
		
		return DB::table('students')
            ->where('id', $id)
            ->update(['name' => $name,'short_name' => $short_name,'sort_order' => $sort_order,'updated_at' => $this->date,'updated_by' => $user_id]);
            
	}
	public function deleteStudent($id)
	{
		$user_id = Auth::id();
		return DB::table('students')
            ->where('id', $id)
            ->update(['status' => '0','updated_at' => $this->date,'updated_by' => $user_id]);
	}
}
