<?php

namespace App\Backend;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Http\Requests;
use Carbon\Carbon;

class Section extends Model
{
	public function __construct()
    {
		$this->date = Carbon::now('Asia/Kolkata');
    }
    public function listSection()
	{
		return DB::table('sections')
			->where([
			['status', '1'],
			])
			->orderBy('name')
            ->get();
	}
	public function addSection($data)
	{
		$user_id = $data['user_id'];
		$name = $data['name'];
		$description = $data['description'];
			
		return DB::table('sections')->insert(
		    ['name' => $name,'description' => $description,'created_at' => $this->date,'created_by' => $user_id]
		);
	}
	public function viewSection($id)
	{
		return DB::table('sections')
			->where([
			['id', $id],
			['status', '1'],
			])
            ->first();
	}
	public function editSection($id)
	{
		return DB::table('sections')
			->where([
			['id', $id],
			['status', '1'],
			])
            ->first();
	}
	public function updateSection($id,$data)
	{
		$user_id = $data['user_id'];
		$name = $data['name'];
		$description = $data['description'];
		
		return DB::table('sections')
            ->where('id', $id)
            ->update(['name' => $name,'description' => $description,'updated_at' => $this->date,'updated_by' => $user_id]);
            
	}
	public function deleteSection($id)
	{
		$user_id = Auth::id();
		return DB::table('sections')
            ->where('id', $id)
            ->update(['status' => '0','updated_at' => $this->date,'updated_by' => $user_id]);
	}
}
