<?php

namespace App\Backend;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Http\Requests;
use Carbon\Carbon;

class Classs extends Model
{
	public function __construct()
    {
		$this->date = Carbon::now('Asia/Kolkata');
    }
    public function listClasss()
	{
		return DB::table('classes')
			->where([
			['status', '1'],
			])
			->orderBy('name')
            ->get();
	}
	public function addClasss($data)
	{
		$user_id = $data['user_id'];
		$name = $data['name'];
		$description = $data['description'];
		$sections = $data['sections'];
			
		return DB::table('classes')->insert(
		    ['name' => $name,'description' => $description,'sections' => $sections,'created_at' => $this->date,'created_by' => $user_id]
		);
	}
	public function viewClasss($id)
	{
		return DB::table('classes')
			->where([
			['id', $id],
			['status', '1'],
			])
            ->first();
	}
	public function editClasss($id)
	{
		return DB::table('classes')
			->where([
			['id', $id],
			['status', '1'],
			])
            ->first();
	}
	public function updateClasss($id,$data)
	{
		$user_id = $data['user_id'];
		$name = $data['name'];
		$description = $data['description'];
		$sections = $data['sections'];
		
		return DB::table('classes')
            ->where('id', $id)
            ->update(['name' => $name,'description' => $description,'sections' => $sections,'updated_at' => $this->date,'updated_by' => $user_id]);
            
	}
	public function deleteClasss($id)
	{
		$user_id = Auth::id();
		return DB::table('classes')
            ->where('id', $id)
            ->update(['status' => '0','updated_at' => $this->date,'updated_by' => $user_id]);
	}
}
