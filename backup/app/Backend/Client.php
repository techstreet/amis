<?php

namespace App\Backend;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Http\Requests;
use Carbon\Carbon;

class Client extends Model
{
	public function __construct()
    {
		$this->date = Carbon::now('Asia/Kolkata');
    }
    public function client_list()
	{
		return DB::table('users')
			->where([
			['is_blocked', '0'],
			['user_group', '3']
			])
            ->get();
	}
}
