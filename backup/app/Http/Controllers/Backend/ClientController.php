<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use DB;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Backend\Client;

class ClientController extends Controller
{
    public function __construct()
    {
		$this->client = new Client();
    }
    public function index()
    {
		$clients = $this->client->client_list();
		$count = $clients->count();
		return view('backend/clients/list',['clients'=>$clients,'count'=>$count]);
    }
}
