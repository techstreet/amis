<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Backend\Section;

class SectionController extends Controller
{
    public function __construct()
    {
		$this->section = new Section();
    }
    public function index()
    {
		$sections = $this->section->listSection();
		return view('backend/sections/list',['sections'=>$sections]);
    }
    public function add()
    {
		return view('backend/sections/add');
    }
    public function save(Request $request)
    {
		$data['user_id'] = Auth::id();
		$data['name'] = $request->input('name');
		$data['description'] = $request->input('description');
		
		$this->validate($request,[
			'name'=>'required|max:255',
			'description'=>'nullable|max:255',
		]);
		
		$result = $this->section->addSection($data);
		
		if($result){
			$request->session()->flash('success', 'Record added successfully!');
		}
		else{
			$request->session()->flash('error', 'Something went wrong!');
		}
		return redirect()->back();
    }
    public function view($id)
    {
		$section = $this->section->viewSection($id);
		return view('backend/sections/view',['section'=>$section]);
    }
    public function edit($id)
    {
		$section = $this->section->editSection($id);
		return view('backend/sections/edit',['section'=>$section]);
    }
    public function update(Request $request,$id)
    {
		$data['user_id'] = Auth::id();
		$data['name'] = $request->input('name');
		$data['description'] = $request->input('description');
		
		$this->validate($request,[
			'name'=>'required|max:255',
			'description'=>'nullable|max:255',
		]);
		
		$result = $this->section->updateSection($id,$data);
		
		if($result){
			$request->session()->flash('success', 'Record updated successfully!');
		}
		else{
			$request->session()->flash('error', 'Something went wrong!');
		}
		return redirect()->back();
    }
    public function delete(Request $request,$id)
    {
		$result = $this->section->deleteSection($id);
		if($result){
			$request->session()->flash('success', 'Record deleted successfully!');
		}
		else{
			$request->session()->flash('error', 'Something went wrong!');
		}
		return redirect()->back();
    }
}
