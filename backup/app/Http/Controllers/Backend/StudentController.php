<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Backend\Student;

class StudentController extends Controller
{
    public function __construct()
    {
		$this->student = new Student();
    }
    public function index()
    {
		$students = $this->student->listStudent();
		return view('backend/students/list',['students'=>$students]);
    }
    public function add()
    {
		return view('backend/students/add');
    }
    public function save(Request $request)
    {
		$data['user_id'] = Auth::id();
		$data['name'] = $request->input('name');
		$data['short_name'] = $request->input('short_name');
		$data['sort_order'] = $request->input('sort_order');
		
		$this->validate($request,[
			'name'=>'required|max:255',
			'short_name'=>'nullable|max:255',
			'sort_order'=>'nullable|integer',
		]);
		
		$result = $this->student->addStudent($data);
		
		if($result){
			$request->session()->flash('success', 'Record added successfully!');
		}
		else{
			$request->session()->flash('error', 'Something went wrong!');
		}
		return redirect()->back();
    }
    public function view($id)
    {
		$student = $this->student->viewStudent($id);
		return view('backend/students/view',['student'=>$student]);
    }
    public function edit($id)
    {
		$student = $this->student->editStudent($id);
		return view('backend/students/edit',['student'=>$student]);
    }
    public function update(Request $request,$id)
    {
		$data['user_id'] = Auth::id();
		$data['name'] = $request->input('name');
		$data['short_name'] = $request->input('short_name');
		$data['sort_order'] = $request->input('sort_order');
		
		$this->validate($request,[
			'name'=>'required|max:255',
			'short_name'=>'nullable|max:255',
			'sort_order'=>'nullable|integer',
		]);
		
		$result = $this->student->updateStudent($id,$data);
		
		if($result){
			$request->session()->flash('success', 'Record updated successfully!');
		}
		else{
			$request->session()->flash('error', 'Something went wrong!');
		}
		return redirect()->back();
    }
    public function delete(Request $request,$id)
    {
		$result = $this->student->deleteStudent($id);
		if($result){
			$request->session()->flash('success', 'Record deleted successfully!');
		}
		else{
			$request->session()->flash('error', 'Something went wrong!');
		}
		return redirect()->back();
    }
}
