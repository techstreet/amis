<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use DB;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class WelcomeController extends Controller
{
    public function __construct()
    {
		//
    }
    public function index()
    {
		return view('backend/welcome');
    }
}
