<?php
if (! function_exists('getSectionName')) {
    function getSectionName($section_id){
		$data = DB::table('sections')
			->where('id',$section_id)
            ->first();
        if(count($data)>0){
			return $data->name;
		}
	}
}
if (! function_exists('date_dfy')) {
    function date_dfy($date){
		if($date == '' || $date == '0000-00-00' || $date == '0000-00-00 00:00:00'){
			return '';
		}
		else{
			return date_format(date_create($date),"d-F-Y");
		}
	}
}
if (! function_exists('date_ymd')) {
    function date_ymd($date){
		if($date == '' || $date == '0000-00-00' || $date == '0000-00-00 00:00:00'){
			return '';
		}
		else{
			return date_format(date_create($date),"Y-m-d");
		}
	}
}
?>