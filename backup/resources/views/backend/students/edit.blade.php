@extends('layouts.backend')
@section('content')
<script src="{{asset('backend/assets/pages/scripts/modernizr.custom.js')}}" type="text/javascript"></script>
<style>
.my-toggle-class {
color: #888;
cursor: pointer;
font-size: 0.75em;
font-weight: bold;
padding: 0.5em 1em;
text-transform: uppercase;
}
</style>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
		<div class="page-bar">
            <ul class="page-breadcrumb breadcrumb">
                <li>
                    <a href="{{url('/')}}"><i class="icon-home"></i> Home</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="{{url('/students')}}">Students</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="{{url('/students/view/'.$student->id)}}">{{$student->name}}</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <span class="active">Edit</span>
                </li>
            </ul>
        </div>
		<div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <span class="caption-subject font-blue-sharp bold uppercase">Edit Student</span>
                </div>
            </div>
            <div class="portlet-body">
                <div class="row">
                	<div class="col-md-6">
                		@include('backend/flashmessage')
                		<form method="post" action="">
	                        <div class="form-body">
	                            {{ csrf_field() }}
							    <div class="form-group">
							      <label>*Name:</label>
							      <input type="text" class="form-control" name="name" id="name" value="{{ $student->name }}" placeholder="Please Enter Name" required="">
							    </div>
							    <div class="form-group">
							      <label>Short Name:</label>
							      <input type="text" class="form-control" name="short_name" id="short_name" value="{{ $student->short_name }}" placeholder="Please Enter Short Name">
							    </div>
							    <div class="form-group">
							      <label>Sort Order:</label>
							      <input type="number" class="form-control" name="sort_order" id="sort_order" value="{{ $student->sort_order }}" min="1" placeholder="Please Enter Sort Order">
							    </div>
	                        </div>
	                        <div class="form-actions">
	                            <button type="submit" class="btn blue">Update</button>
	                            <button type="button" class="btn default" onclick="location.href = '{{url('/students')}}';">Cancel</button>
	                        </div>
	                    </form>
                	</div>
                </div>
            </div>
	    </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
<!-- Include the plugin. Yay! --> 
<script src="{{asset('backend/assets/pages/scripts/hideShowPassword.min.js')}}" type="text/javascript"></script>
<script>
// Example 2
$('#old_password,#password,#confirm_password').hideShowPassword({
  // Make the password visible right away.
  show: false,
  // Create the toggle goodness.
  innerToggle: true,
  // Give the toggle a custom class so we can style it
  // separately from the previous example.
  toggleClass: 'my-toggle-class',
  // Don't show the toggle until the input triggers
  // the 'focus' event.
  hideToggleUntil: 'focus',
  // Enable touch support for toggle.
  touchSupport: Modernizr.touch
});
</script>
@endsection